<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password', 'role_id', 'name', 'surname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function isAdmin()
    {
        return $this->role && $this->role->isAdmin();
    }

    public function getUsernameAttribute()
    {
        if ($this->name && $this->surname) {
            return $this->fullname;
        }

        return $this->login;
    }

    public function getFullnameAttribute()
    {
        return $this->surname . ' ' . $this->name;
    }

    public static function collection()
    {
        return User::all();
    }

    public function updateProfile($name, $surname, $login = null, $email = null)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->login = $login ?: $this->login;
        $this->email = $email ?: $this->email;
        $this->save();
        return $this;
    }

    public function createCategory($name, $parentId = null)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        return Category::create([
            'name' => $name,
            'slug' => slug($name),
            'parent_id' => $parentId
        ]);
    }

    public function updateCategory($id, $name)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        return Category::find($id)
            ->update([
                'name' => $name,
                'slug' => slug($name)
            ]);
    }

    public function removeCategory($id)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        return Category::destroy($id);
    }

    public function createPost($categoryId, $title, $content, $author = null)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        $publication = Publication::today();
        if (!$publication) {
            $publication = Publication::create([
                'name' => today()->format('d-m-y')
            ]);
        }

        $author = $author ?: $this->username;

        return Post::create([
            'category_id' => $categoryId,
            'publication_id' => $publication->id,
            'author' => $author,
            'user_id' => $this->id,
            'title' => $title,
            'content' => $content
        ]);
    }

    public function updatePost($id, $categoryId, $title, $content, $author = null)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        $author = $author ?: $this->username;

        return Post::find($id)->update([
            'category_id' => $categoryId,
            'title' => $title,
            'content' => $content,
            'author' => $author
        ]);
    }

    public function removePost($id)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        return Post::destroy($id);
    }

    public function createComment($postId, $content)
    {
//        TODO find better way to check if guest
        if (!$this->role) {
//            TODO return error message
            return null;
        }

        return Comment::create([
            'author_id' => $this->id,
            'post_id' => $postId,
            'content' => $content
        ]);
    }

    public function removeComment($id)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        return Comment::destroy($id);
    }

    public function createTranslation($content, $categoryId, $parentId = null)
    {
        if (!$this->isAdmin()) {
//            TODO return error message
            return null;
        }

        return Translation::create([
            'content' => $content,
            'category_id' => $categoryId,
            'parent_id' => $parentId,
        ]);
    }
}
