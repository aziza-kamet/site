<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public static function adminId()
    {
        return self::where('name', 'admin')->first()->id;
    }

    public static function userId()
    {
        return self::where('name', 'user')->first()->id;
    }

    public function isAdmin()
    {
        return $this->name === 'admin';
    }
}
