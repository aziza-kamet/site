<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'category_id',
        'publication_id',
        'user_id',
        'title',
        'author',
        'content'
    ];

    protected $with = ['category', 'publication'];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function publication()
    {
        return $this->belongsTo(Publication::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getPostedByAttribute()
    {
        return $this->author;
    }

    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/y H:i:s');
    }

    public static function collection()
    {
        return self::all();
    }
}
