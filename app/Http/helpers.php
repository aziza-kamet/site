<?php

function user(): \App\User
{
    return \Auth::user() ?: new \App\User();
}

function slug($text, $glue = '_')
{
    return Slug::make($text, $glue);
}

function substrTextOnly($string, $limit, $end='...')
{
    $withHtmlCount = strlen($string);
    $withoutHtmlCount = strlen(strip_tags($string));
    $htmlTagsLength = $withHtmlCount-$withoutHtmlCount;

    if (strpos($string, '<table') !== false) {
        preg_match_all('/<tr>.*?<\/tr>/s', $string, $matches);
        $length = count($matches[0]) > 10 ? 10 : count($matches[0]) - 1;
        $needle = $matches[0][$length];
        $result = substr($string, 0, strpos($string, $needle));
        return $result . '</tr></table>';
    }

    return str_limit($string, $limit + $htmlTagsLength, $end);
}
