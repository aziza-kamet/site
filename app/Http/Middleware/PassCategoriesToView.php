<?php

namespace App\Http\Middleware;

use App\Category;
use Closure;
use Illuminate\Filesystem\Cache;

class PassCategoriesToView
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Cache::has('menu_categories')) {
            \Cache::put('menu_categories', Category::roots(), config('constants.minutes.month'));
        }
        return $next($request);
    }
}
