<?php

namespace App\Http\Middleware;

use Closure;

class MustBeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!(auth()->check() && user()->isAdmin())) {
            return redirect()->guest('/')->withErrors([
                'error' => 'User must be admin'
            ]);
        }
        return $next($request);
    }
}
