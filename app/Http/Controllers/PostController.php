<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $categories = Category::collection();
        $posts = Post::collection();
        return view('admin.posts.index', compact('categories', 'posts'));
    }

    public function store()
    {
        $this->validate(request(), [
            'category' => 'required|numeric|exists:categories,id',
            'title' => 'required|max:300',
            'content' => 'required',
        ]);

        user()->createPost(
            request('category'),
            request('title'),
            request('content'),
            request('author')
        );

        return redirect()->route('admin.posts.index');
    }

    public function show($id)
    {
        $post = Post::with('comments')->find($id);

        if (user()->isAdmin()) {
            return view('admin.posts.show', compact('post'));
        }

        return view('posts.show', compact('post'));
    }

    public function jsonShow($id)
    {
        return Post::find($id)->toJson();
    }

    public function update($id)
    {
        $this->validate(request(), [
            'category' => 'required|numeric|exists:categories,id',
            'title' => 'required|max:300',
            'content' => 'required'
        ]);

        user()->updatePost(
            $id,
            request('category'),
            request('title'),
            request('content'),
            request('author')
        );

        return redirect()->route('admin.posts.index');
    }

    public function destroy($id)
    {
        user()->removePost($id);

        return redirect()->route('admin.posts.index');
    }
}
