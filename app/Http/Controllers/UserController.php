<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
//        TODO check if user is admin
        $users = User::collection();
        return view('admin.users.index', compact('users'));
    }

    public function update()
    {
        $this->validate(request(), [
            'name' => 'nullable|alpha',
            'surname' => 'nullable|alpha'
        ]);

        user()->updateProfile(request('name'), request('surname'));

        return redirect()->route('profile');
    }

    public function updateAdmin()
    {
        $this->validate(request(), [
            'login' => 'nullable|string|max:50|unique:users,login,' . user()->id,
            'email' => 'required|string|email|max:255|unique:users,email,' . user()->id,
            'name' => 'nullable|alpha',
            'surname' => 'nullable|alpha'
        ]);

        user()->updateProfile(
            request('name'),
            request('surname'),
            request('login'),
            request('email')
        );

        return redirect()->route('admin.dashboard');
    }
}
