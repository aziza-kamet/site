<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store($pid)
    {
        if (auth()->guest()) {
//            TODO return error message
            return redirect()->back();
        }

        $this->validate(request(), [
            'content' => 'required|max:300'
        ]);

        user()->createComment($pid, request('content'));

        return redirect()->back();
    }

    public function destroy($id)
    {
        if (!user()->isAdmin()) {
            return null;
        }

        $postId = Comment::find($id)->post_id;
        Comment::destroy($id);
        return redirect()->route('admin.posts.show', $postId);
    }
}
