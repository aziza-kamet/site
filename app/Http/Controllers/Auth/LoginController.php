<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function rules()
    {
        return [
            'login' => 'required',
            'password' => 'required'
        ];
    }

    public function login()
    {
        $field = filter_var(request()->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'login';
        request()->merge([$field => request()->input('username')]);

        if (Auth::attempt(request()->only($field, 'password'))) {
            if (user()->isAdmin()) {
                return redirect(route('admin.dashboard'));
            }
            return redirect(route('profile'));
        }

        return redirect('/login')->withErrors([
            'error' => 'These credentials do not match our records.',
        ]);
    }
}
