<?php

namespace App\Http\Controllers;

use App\Category;
use App\Translation;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        if (user()->isAdmin()) {
            $categories = Category::collection();
            $roots = Category::roots(true);
            return view('admin.categories.index', compact('categories', 'roots'));
        }
    }

    public function store()
    {
//        TODO check if user is admin
        $rules = [
            'name' => 'required',
            'parent' => 'numeric'
        ];

//        TODO should refactor
        if (request('parent') !== '0') {
            $rules['parent'] .= '|exists:categories,id';
            $parent = request('parent');
        } else {
            $parent = null;
        }

        $this->validate(request(), $rules);

        $name = request('name');
        user()->createCategory($name, $parent);

        return redirect()->route('admin.categories.index');
    }

//    TODO replace category to id
    public function update($id)
    {
//        TODO check if user is admin
        $rules = [
            'name' => 'required'
        ];
        $this->validate(request(), $rules);

        user()->updateCategory($id, request('name'));

        return redirect()->route('admin.categories.index');
    }

//    TODO replace category to id
    public function destroy($id)
    {
        user()->removeCategory($id);

        return redirect()->route('admin.categories.index');
    }

    public function posts($id)
    {
        $category = Category::find($id);
        $posts = $category->posts;

        return view('categories.posts', compact('category', 'posts'));
    }
}
