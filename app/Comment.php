<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'content', 'author_id', 'post_id'
    ];

    protected $with = [
        'author'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/y H:i:s');
    }

    public function getLeftByAttribute()
    {
        return $this->author->username;
    }
}
