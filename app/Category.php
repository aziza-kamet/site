<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'slug', 'parent_id'];
    protected $with = ['parent'];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function hasParent()
    {
        return $this->parent_id !== null;
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function hasChildren()
    {
        return $this->children()->get()->isNotEmpty();
    }

    public static function collection()
    {
        return self::all();
    }

    public static function roots($withChildren = false)
    {
        $result = self::where('parent_id', null);
        if ($withChildren) {
            return $result->with('children')->get();
        }
        return $result->get();
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public static function translationCategory()
    {
        return self::where(
            'name',
            config('constants.categories')[1]
        )->first();
    }
}
