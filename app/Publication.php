<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    protected $fillable = ['name'];

    public static function today()
    {
        return Publication::whereDate('created_at', \DB::raw('CURDATE()'))->first();
    }

    public static function existsToday()
    {
        return self::today() !== null;
    }
}
