<?php

namespace Tests;

use App\Role;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp()
    {
        parent::setUp();

        Role::insert([
            ['name' => 'admin'],
            ['name' => 'user'],
        ]);

        $categoryNames = config('constants.categories');

        $categories = array_map(function ($name) {
            return [
                'name' => $name,
                'slug' => slug($name)
            ];
        }, $categoryNames);

        \App\Category::insert($categories);
    }

    public function admin(): User
    {
        return factory(User::class)->create([
            'role_id' => Role::adminId()
        ]);
    }

    public function user(): User
    {
        return factory(User::class)->create();
    }

    public function faker()
    {
        return $faker = Faker::create();
    }
}
