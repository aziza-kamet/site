<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeViewedByAdmin()
    {
        [$userA, $userB] = factory(User::class, 2)->create();
        $this->actingAs($this->admin())
            ->get(route('admin.users.index'))
            ->assertSee($userA->login)
            ->assertSee($userB->login);
    }

    public function testCanUpdateNameAndSurname()
    {
        $user = $this->user();
        $name = $this->faker()->firstName;
        $surname = $this->faker()->lastName;
        $this->actingAs($user)
            ->put(route('users.update'), [
                'name' => $name,
                'surname' => $surname
            ]);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => $name,
            'surname' => $surname
        ]);
    }

    public function testAdminCanUpdateFullProfile()
    {
        $admin = $this->admin();
        $parameters = [
            'login' => $this->faker()->userName,
            'email' => $this->faker()->email,
            'name' => $this->faker()->firstName,
            'surname' => $this->faker()->lastName,
        ];

        $this->actingAs($admin)
            ->put(route('admin.update'), $parameters);

        $this->assertDatabaseHas('users', array_merge($parameters, [
            'id' => $admin->id
        ]));
    }
}
