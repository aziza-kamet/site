<?php

namespace Tests\Feature;

use App\Comment;
use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeCreatedByAuthenticatedUser()
    {
        $user = $this->user();
        $post = factory(Post::class)->create();
        $content = $this->faker()->sentence;
        $this->actingAs($user)
            ->post(route('post.comments.store', $post->id), [
                'content' => $content
            ]);

        $this->assertDatabaseHas('comments', [
            'author_id' => $user->id,
            'post_id' => $post->id,
            'content' => $content
        ]);
    }

    public function testCanBeViewed()
    {
        $comment = factory(Comment::class)->create();

        $this->get(route('posts.show', $comment->post_id))
            ->assertSee($comment->content);
    }

    public function testCanBeViewedByAdmin()
    {
        $admin = $this->admin();
        $comment = factory(Comment::class)->create();

        $this->actingAs($admin)
            ->get(route('admin.posts.show', $comment->post_id))
            ->assertSee($comment->content);
    }

    public function testCanBeRemovedByAdmin()
    {
        $admin = $this->admin();
        $comment = factory(Comment::class)->create();

        $this->actingAs($admin)
            ->delete(route('admin.comments.destroy', $comment->id));

        $this->assertDatabaseMissing('comments', [
            'id' => $comment->id
        ]);
    }
}
