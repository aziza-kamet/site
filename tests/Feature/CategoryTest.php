<?php

namespace Tests\Feature;

use App\Category;
use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeCreatedByAdmin()
    {
        $admin = $this->admin();
        $categoryName = $this->faker()->word;
        $this->actingAs($admin)
            ->post(route('admin.categories.store'), [
                'name' => $categoryName,
                'parent' => '0'
            ]);
        $this->assertDatabaseHas('categories', [
            'name' => $categoryName,
            'parent_id' => null
        ]);
    }

    public function testCanBeCreatedWithParentByAdmin()
    {
        $admin = $this->admin();
        $parentCategory = factory(Category::class)->create();
        $categoryName = $this->faker()->word;
        $this->actingAs($admin)
            ->post(route('admin.categories.store'), [
                'name' => $categoryName,
                'parent' => $parentCategory->id
            ]);
        $this->assertDatabaseHas('categories', [
            'name' => $categoryName,
            'parent_id' => $parentCategory->id
        ]);
    }

    public function testCannotBeCreatedByOthers()
    {
        $user = $this->user();
        $categoryName = $this->faker()->word;
        $this->actingAs($user)
            ->post(route('admin.categories.store'), [
                'name' => $categoryName,
                'parent' => '0'
            ]);
        $this->assertDatabaseMissing('categories', [
            'name' => $categoryName,
            'parent_id' => null
        ]);
    }

    public function testCollectionCanBeViewedByAdmin()
    {
        [$categoryA, $categoryB] = factory(Category::class, 2)->create();
        $admin = $this->admin();
        $this->actingAs($admin)
            ->get(route('admin.categories.index'))
            ->assertSee($categoryA->name)
            ->assertSee($categoryB->name);
    }

    public function testCanBeRemovedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $this->actingAs($admin)
            ->delete(route('admin.categories.destroy', $category->id));

        $this->assertDatabaseMissing('categories', [
            'name' => $category->name
        ]);
    }

    public function testCannotBeRemovedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $this->actingAs($user)
            ->delete(route('admin.categories.destroy', $category->id));

        $this->assertDatabaseHas('categories', [
            'name' => $category->name
        ]);
    }

    public function testCanBeUpdatedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $categoryName = $this->faker()->word;

        $this->actingAs($admin)
            ->put(route('admin.categories.update', $category->id), [
                'name' => $categoryName
            ]);

        $this->assertDatabaseHas('categories', [
            'name' => $categoryName
        ]);
    }

    public function testCannotBeUpdatedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $categoryName = $this->faker()->word;

        $this->actingAs($user)
            ->put(route('admin.categories.update', $category->id), [
                'name' => $categoryName
            ]);

        $this->assertDatabaseMissing('categories', [
            'name' => $categoryName
        ]);
    }

    public function testUsersPostsCanBeDisplayedForChosenCategory()
    {
        $post = factory(Post::class)->create();
        $this->get(route('category.posts', $post->category_id))
            ->assertSee($post->title);
    }
}
