<?php

namespace Tests\Feature;

use App\Category;
use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeCreatedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->text;

        $this->actingAs($admin)
            ->post(route('admin.posts.store'), [
                'category' => $category->id,
                'title' => $postTitle,
                'content' => $postContent
            ]);

        $this->assertDatabaseHas('posts', [
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent
        ]);
    }

    public function testCanBeCreatedWithAuthor()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->text;
        $postAuthor = $this->faker()->name;

        $this->actingAs($admin)
            ->post(route('admin.posts.store'), [
                'category' => $category->id,
                'title' => $postTitle,
                'content' => $postContent,
                'author' => $postAuthor
            ]);

        $this->assertDatabaseHas('posts', [
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
            'author' => $postAuthor
        ]);
    }

    public function testCannotBeCreatedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->text;

        $this->actingAs($user)
            ->post(route('admin.posts.store'), [
                'category' => $category->id,
                'title' => $postTitle,
                'content' => $postContent
            ]);

        $this->assertDatabaseMissing('posts', [
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent
        ]);
    }

    public function testCanBeViewedByAdmin()
    {
        $admin = $this->admin();
        [$postA, $postB] = factory(Post::class, 2)->create([
            'user_id' => $admin->id,
            'author' => $admin->username
        ]);

        $this->actingAs($admin)
            ->get(route('admin.posts.index'))
            ->assertSee($postA->title)
            ->assertSee($postB->title);
    }

    public function testCanBeUpdatedByAdmin()
    {
        $admin = $this->admin();
        $post = factory(Post::class)->create([
            'user_id' => $admin->id,
            'author' => $admin->username
        ]);
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->text;
        $postAuthor = $this->faker()->name;

        $this->actingAs($admin)
            ->put(route('admin.posts.update', $post->id), [
                'category' => $category->id,
                'title' => $postTitle,
                'content' => $postContent,
                'author' => $postAuthor
            ]);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
            'author' => $postAuthor
        ]);
    }

    public function testCanBeRemovedByAdmin()
    {
        $admin = $this->admin();
        $post = factory(Post::class)->create([
            'user_id' => $admin->id,
            'author' => $admin->username
        ]);

        $this->actingAs($admin)
            ->delete(route('admin.posts.destroy', $post->id));

        $this->assertDatabaseMissing('posts', [
            'category_id' => $post->category_id,
            'title' => $post->title,
            'content' => $post->content
        ]);
    }

    public function testCannotBeRemovedByOthers()
    {
        $user = $this->user();
        $post = factory(Post::class)->create([
            'user_id' => $this->admin()->id
        ]);

        $this->actingAs($user)
            ->delete(route('admin.posts.destroy', $post->id));

        $this->assertDatabaseHas('posts', [
            'category_id' => $post->category_id,
            'title' => $post->title,
            'content' => $post->content
        ]);
    }

    public function testSinglePostCanBeViewedByAdmin()
    {
        $admin = $this->admin();
        $post = factory(Post::class)->create();

        $this->actingAs($admin)
            ->get(route('admin.posts.show', $post->id))
            ->assertSee($post->title);
    }

    public function testSinglePostCanBeViewed()
    {
        $post = factory(Post::class)->create();

        $this->get(route('posts.show', $post->id))
            ->assertSee($post->title);
    }
}
