<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testCanUpdateNameAndSurname()
    {
        $user = $this->user();
        $name = $this->faker()->firstName;
        $surname = $this->faker()->lastName;

        $user->updateProfile($name, $surname);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => $name,
            'surname' => $surname
        ]);
    }

    public function testAdminCanUpdateFullProfile()
    {
        $admin = $this->admin();
        $login = $this->faker()->userName;
        $email = $this->faker()->email;
        $name = $this->faker()->firstName;
        $surname = $this->faker()->lastName;

        $admin->updateProfile($name, $surname, $login, $email);
        $this->assertDatabaseHas('users', [
            'id' => $admin->id,
            'login' => $login,
            'email' => $email,
            'name' => $name,
            'surname' => $surname,
        ]);
    }
}
