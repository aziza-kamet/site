<?php

namespace Tests\Unit;

use App\Category;
use App\Post;
use App\Publication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeCreatedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->paragraph;
        $admin->createPost($category->id, $postTitle, $postContent);

        $this->assertDatabaseHas('posts', [
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
        ]);
    }

    public function testCanBeCreatedWithCustomAuthor()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->paragraph;
        $postAuthor = $this->faker()->name;
        $admin->createPost($category->id, $postTitle, $postContent, $postAuthor);

        $this->assertDatabaseHas('posts', [
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
            'author' => $postAuthor
        ]);
    }

    public function testPublicationWontBeDuplicated()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        factory(Publication::class)->create();
        $admin->createPost(
            $category->id,
            $this->faker()->sentence,
            $this->faker()->paragraph
        );

        self::assertCount(1, Publication::whereDate(
            'created_at',
            \DB::raw('CURDATE()')
        )->get());
    }

    public function testCannotBeCreatedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->paragraph;
        $user->createPost($category->id, $postTitle, $postContent);

        $this->assertDatabaseMissing('posts', [
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
        ]);
    }

    public function testCanBeUpdatedByAdmin()
    {
        $admin = $this->admin();
        $post = factory(Post::class)->create([
            'user_id' => $admin->id,
            'author' => $this->admin()->username
        ]);
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->text;
        $postAuthor = $this->faker()->name;
        $admin->updatePost($post->id, $category->id, $postTitle, $postContent, $postAuthor);

        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
            'author' => $postAuthor
        ]);
    }

    public function testCannotBeUpdatedByOthers()
    {
        $user = $this->user();
        $post = factory(Post::class)->create([
            'user_id' => $this->admin()->id,
            'author' => $this->admin()->username
        ]);
        $category = factory(Category::class)->create();
        $postTitle = $this->faker()->sentence;
        $postContent = $this->faker()->text;
        $user->updatePost($post->id, $category->id, $postTitle, $postContent);

        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
            'category_id' => $category->id,
            'title' => $postTitle,
            'content' => $postContent,
        ]);
    }

    public function testCanBeRemovedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $post = $admin->createPost(
            $category->id,
            $this->faker()->sentence,
            $this->faker()->text
        );
        $admin->removePost($post->id);

        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
    }

    public function testCannotBeRemovedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $post = $this->admin()->createPost(
            $category->id,
            $this->faker()->sentence,
            $this->faker()->text
        );
        $user->removePost($post->id);

        $this->assertDatabaseHas('posts', ['id' => $post->id]);
    }
}
