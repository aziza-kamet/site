<?php

namespace Tests\Unit;

use App\Comment;
use App\Post;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeCreatedByAuthenticatedUser()
    {
        $user = $this->user();
        $post = factory(Post::class)->create();
        $content = $this->faker()->sentence;
        $user->createComment($post->id, $content);

        $this->assertDatabaseHas('comments', [
            'author_id' => $user->id,
            'post_id' => $post->id,
            'content' => $content
        ]);
    }

    public function testCannotBeCreatedByGuest()
    {
        $user = new User;
        $post = factory(Post::class)->create();
        $content = $this->faker()->sentence;
        $user->createComment($post->id, $content);

        $this->assertDatabaseMissing('comments', [
            'post_id' => $post->id,
            'content' => $content
        ]);
    }

    public function testCanBeRemovedByAdmin()
    {
        $admin = $this->admin();
        $comment = factory(Comment::class)->create();
        $admin->removeComment($comment->id);

        $this->assertDatabaseMissing('comments', [
            'id' => $comment->id,
        ]);
    }
}
