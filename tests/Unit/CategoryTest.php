<?php

namespace Tests\Unit;

use App\Category;
use App\Role;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBeCreatedByAdmin()
    {
        $admin = $this->admin();
        $categoryName = $this->faker()->word;
        $admin->createCategory($categoryName);

        $this->assertDatabaseHas('categories', ['name' => $categoryName]);
    }

    public function testCanBeCreatedWithParentByAdmin()
    {
        $admin = $this->admin();
        $parentCategory = $admin->createCategory('parent');
        $categoryName = $this->faker()->word;
        $admin->createCategory($categoryName, $parentCategory->id);

        $this->assertDatabaseHas('categories', [
            'name' => $categoryName,
            'parent_id' => $parentCategory->id
        ]);
    }

    public function testCannotBeCreatedByOtherRoles()
    {
        $user = $this->user();
        $categoryName = $this->faker()->word;
        $user->createCategory($categoryName);

        $this->assertDatabaseMissing('categories', ['name' => $categoryName]);
    }

    public function testCanBeUpdatedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $categoryName = $this->faker()->word;
        $admin->updateCategory($category->id, $categoryName);

        $this->assertDatabaseHas('categories', ['name' => $categoryName]);
    }

    public function testCannotBeUpdatedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $categoryName = $this->faker()->word;
        $user->updateCategory($category->id, $categoryName);

        $this->assertDatabaseMissing('categories', ['name' => $categoryName]);
    }

    public function testCanBeRemovedByAdmin()
    {
        $admin = $this->admin();
        $category = factory(Category::class)->create();
        $admin->removeCategory($category->id);

        $this->assertDatabaseMissing('categories', ['name' => $category->name]);
    }

    public function testCannotBeRemovedByOthers()
    {
        $user = $this->user();
        $category = factory(Category::class)->create();
        $user->removeCategory($category);

        $this->assertDatabaseHas('categories', ['name' => $category->name]);
    }
}
