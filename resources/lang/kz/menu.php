<?php

return [
    'home' => 'Басты бет',
    'categories' => 'Салалар',
    'profile' => 'Профиль',
    'posts' => 'Мақалалар',
    'users' => 'Қолданушылар',
    'login' => 'Кіру',
    'sign_up' => 'Тіркелу',
    'logout' => 'Шығу',
];