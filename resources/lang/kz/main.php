<?php

return [
    'home' => 'Басты бет',
    'data_missing' => 'Деректер енгізілмеген',

    'profile' => 'Профиль',
    'users' => 'Қолданушылар',
    'email_or_login' => 'Пошта немесе логин',
    'username' => 'Лақап ат',
    'email' => 'Пошта',
    'password' => 'Құпиясөз',
    're_password' => 'Құпиясөзді қайта теріңіз',
    'name' => 'Есіміңіз',
    'surname' => 'Тегіңіз',
    'fullname' => 'Аты-жөні',
    'role' => 'Рөлі',

    'login' => 'Кіру',
    'sign_up' => 'Тіркелу',
    'save' => 'Сақтау',

    'categories' => 'Салалар',
    'naming' => 'Атауы',
    'category' => 'Саласы',
    'non_category' => 'Ешбір салаға кірмейді',
    'edit_category' => 'Саланы өзгерту',

    'posts' => 'Мақалалар',
    'title' => 'Тақырыбы',
    'publication_number' => 'Қойма нөмірі',
    'author' => 'Авторы',
    'date' => 'Мерзімі',
    'edit_post' => 'Мақаланы өзгерту',
    'read_more' => 'жалғасын оқу',
];