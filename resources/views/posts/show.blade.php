@extends('layouts.app')

@section('title')
    {{ $post->title }}
@endsection

@section('content')
    <div class="with-bg-img" style="background: url({{ url('storage/images/' . $post->category->slug . '.jpg') }});"></div>
    <div class="container">
        <div class="card post">
            <div class="card-body">
                <h3 class="card-title">{{ $post->title }}</h3>
                <h6 class="card-subtitle mb-2 text-muted">Автор: {{ $post->postedBy }}</h6>
                <div class="card-text table-responsive">{!! $post->content !!}</div>
                <small class="text-muted">{{ $post->date }}</small>
            </div>
        </div>

        <div class="list-group">
            @include('comments.show', $post)
        </div>
    </div>
@endsection