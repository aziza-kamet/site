@extends('layouts.app')

@section('title')
    Профиль
@endsection

@section('content')
    <div class="row d-flex justify-content-center align-items-center" style="height:90vh;">
        <div class="col-md-4">
            <div class="">
                <div class="panel-body">
                    <form method="POST" action="{{ route('users.update') }}">
                        <div class="form-group">
                            <label for="username">{{ __('main.username') }}</label>
                            <input id="username" type="text" class="form-control" value="{{ user()->login }}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="email">{{ __('main.email') }}</label>
                            <input id="email" type="text" class="form-control" value="{{ user()->email }}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="name">{{ __('main.name') }}</label>
                            <input id="name" type="text" class="form-control" name="name" value="{{ user()->name }}">
                        </div>
                        <div class="form-group">
                            <label for="surname">{{ __('main.surname') }}</label>
                            <input id="surname" type="text" class="form-control" name="surname" value="{{ user()->surname }}">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ __('main.save') }}
                            </button>
                        </div>

                        {{ csrf_field() }}
                        {{ method_field('put') }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
