@extends('layouts.app')

@section('title')
    {{ __('main.home') }}
@endsection

@section('content')
    @include('categories.index')
@endsection