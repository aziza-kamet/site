@if(auth()->check())
    <div class="list-group-item flex-column align-items-start">
        <form action="{{ route('post.comments.store', $post->id) }}" method="post">
            <div class="form-group">
                <label for="comment">Ой-пікіріңізді қалдырыңыз:</label>
                <textarea class="form-control" id="comment" name="content" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Жіберу</button>
            {{ csrf_field() }}
        </form>
    </div>
@else
    <div class="alert alert-warning" role="alert">
        Өз пікіріңізді қалдыру үшін аккаунтыңызға кіруіңіз қажет
    </div>
@endif
@foreach($post->comments as $comment)
    <div class="list-group-item flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
            <p class="mb-1">{{ $comment->content }}</p>
            <small>{{ $comment->date }}</small>
            @if(user()->isAdmin())
                <form action="{{ route('admin.comments.destroy', $comment->id) }}" method="post">
                    <button class="btn btn-danger">x</button>
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                </form>
            @endif
        </div>
        <small class="text-muted">{{ $comment->leftBy }}</small>
    </div>
@endforeach