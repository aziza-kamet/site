<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/"><b>Turki</b>.kz</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}"> {{ __('menu.home') }} <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('menu.categories') }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach(Cache::get('menu_categories') as $category)
                            <a class="dropdown-item" href="{{ route('category.posts', $category->id) }}">{{ $category->name }}</a>
                        @endforeach
                    </div>
                </li>
                @if(auth()->check())
                    <li class="nav-item">
                        <a class="nav-link" href="
                            @if(user()->isAdmin())
                                {{ route('admin.dashboard') }}
                            @else
                                {{ route('profile') }}
                            @endif
                        "> {{ __('menu.profile') }} </a>
                    </li>
                @endif
            </ul>
            {{--<form class="form-inline">--}}
                {{--<input class="form-control mr-sm-2" type="search" placeholder="Сөзді енгізіңіз..." aria-label="Search">--}}
                {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Іздеу</button>--}}
            {{--</form>--}}
            <ul class="navbar-nav ml-auto">
                @if (Route::has('login'))
                    @guest
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('login') }}"> {{ __('menu.login') }} </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('register') }}"> {{ __('menu.sign_up') }} </a>
                        </li>
                    @else
                        <li class="nav-item active">
                            <button class="nav-link" form="logout-form"> {{ __('menu.logout') }} </button>
                            <form id="logout-form" action="{{ route('logout') }}" method="post">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endguest
                @endif
            </ul>
        </div>
    </nav>
</div>

    @yield('content')

    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    @yield('scripts')
</body>
</html>
