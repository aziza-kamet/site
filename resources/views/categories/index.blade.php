<div class="container">
    <div class="row grid d-flex justify-content-center">
        @foreach($categories as $category)
            <div class="col-sm-3">
                <div class="card text-white">
                    <img class="card-img" src="{{ url('storage/images/' . $category->slug . '.jpg') }}" alt="Card image">
                    <a href="{{ route('category.posts', $category->id) }}">
                        <div class="card-img-overlay d-flex align-items-center justify-content-center">
                            <h2 class="card-title">{{ $category->name }}</h2>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>