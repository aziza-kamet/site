@extends('layouts.app')

@section('title')
    {{ $category->name }}
@endsection

@section('content')
    <div class="with-bg-img" style="background: url({{ url('storage/images/' . $category->slug . '.jpg') }});"></div>
    <div class="container posts">
        <div class="card post">
            <div class="card-body">
                <h2 class="text-center">{{ $category->name }}</h2>
            </div>
        </div>

        @foreach($posts as $post)
            <div class="card post">
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ route('posts.show', $post->id) }}">{{ $post->title }}</a></h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ __('main.author') }}: {{ $post->postedBy }}</h6>
                    <p class="card-text table-responsive">
                        {!! substrTextOnly($post->content, 100) !!}
                    </p>
                    <a href="{{ route('posts.show', $post->id) }}"> {{ __('main.read_more') }}</a>
                    <p class="text-right"><small class="text-muted">{{ $post->date }}</small></p>
                </div>
            </div>
        @endforeach
    </div>
@endsection