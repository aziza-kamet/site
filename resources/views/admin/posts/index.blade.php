@extends('layouts.admin')

@section('title')
    {{ __('main.posts') }}
@endsection

@section('content')
    @include('admin.posts.partials.form', [
        'type' => config('constants.types.forms.add'),
        'action' => route('admin.posts.store')
    ])
    <br>
    <div>
        <table class="table">
            <tr>
                <th scope="col">{{ __('main.publication_number') }}</th>
                <th scope="col">{{ __('main.title') }}</th>
                <th scope="col">{{ __('main.author') }}</th>
                <th scope="col">{{ __('main.category') }}</th>
                <th scope="col">{{ __('main.date') }}</th>
                <th scope="col"></th>
            </tr>
            @forelse($posts as $post)
                <tr>
                    <td scope="row">№{{ $post->publication->id }}</td>
                    <td><a href="{{ route('admin.posts.show', $post->id) }}">{{ $post->title }}</a></td>
                    <td>{{ $post->postedBy }}</td>
                    <td>{{ $post->category->name }}</td>
                    <td>{{ $post->date }}</td>
                    <td>
                        <form action="{{ route('admin.posts.destroy', $post->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button class="btn btn-danger">
                                <span class="oi oi-x" title="Өшіру" aria-hidden="true"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">{{ __('main.data_missing') }}</td>
                </tr>
            @endforelse
        </table>
    </div>
@endsection

{{--@section('scripts')--}}
    {{--<script>--}}
        {{--$('#editModal').on('show.bs.modal', function (event) {--}}
            {{--var button = $(event.relatedTarget);--}}
            {{--var id = button.data('id');--}}
            {{--var form = $(this).find('form');--}}

            {{--$.ajax({--}}
                {{--type: 'get',--}}
                {{--url: '/json/posts/' + id,--}}
                {{--dataType: 'json',--}}
                {{--success: function (data) {--}}
                    {{--form.find('input[name="title"]').val(data.title);--}}
                    {{--form.find('input[name="author"]').val(data.author);--}}
                    {{--form.find('select[name="category"]').val(data.category_id);--}}
                    {{--form.find('textarea[name="content"]').html(data.content);--}}
                    {{--form.attr('action', '/admin/posts/' + data.id);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}