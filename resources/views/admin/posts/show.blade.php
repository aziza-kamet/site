@extends('layouts.admin')

@section('title')
    {{ $post->title }}
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{ $post->title }}</h5>
            <h6 class="card-subtitle mb-2 text-muted">Автор: {{ $post->postedBy }}</h6>
            <div class="card-text table-responsive">{!! $post->content !!}</div>
            <small class="text-muted">{{ $post->date }}</small>
        </div>
    </div>

    <br>
    <div class="list-group">
        @include('comments.show', $post)
    </div>
@endsection