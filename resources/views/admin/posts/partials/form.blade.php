<form action="{{ isset($action) ? $action : null }}" method="post">
    <div class="form-group">
        <label for="category">{{ __('main.category') }}</label>
        <select class="form-control" id="category" name="category">
            @foreach($categories as $category)
                <option value="{{ $category->id }}">
                    {{ $category->name }}
                    @if($category->hasParent())
                        ({{ $category->parent->name }})
                    @endif
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="title">{{ __('main.title') }}</label>
        <input type="text" class="form-control" id="title" name="title">
    </div>
    <div class="form-group">
        <label for="author">{{ __('main.author') }}</label>
        <input type="text" class="form-control" id="author" name="author" placeholder="{{ user()->username }}">
    </div>
    <div class="form-group">
        <textarea class="form-control" id="editor" name="content"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">{{ __('main.save') }}</button>
    {{ csrf_field() }}
    @if( $type === config('constants.types.forms.edit') )
        {{ method_field('put') }}
    @endif
</form>