@extends('layouts.admin')

@section('title')
    {{ __('main.users') }}
@endsection

@section('content')
    <table class="table">
        <tr>
            <th class="col">{{ __('main.username') }}</th>
            <th class="col">{{ __('main.email') }}</th>
            <th class="col">{{ __('main.fullname') }}</th>
            <th class="col">{{ __('main.role') }}</th>
        </tr>
        @forelse($users as $user)
            <tr>
                <td>{{ $user->login }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->fullname }}</td>
                <td>{{ $user->role->name }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="4"></td>
            </tr>
        @endforelse
    </table>
@endsection