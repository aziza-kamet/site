@extends('layouts.admin')

@section('content')
    <form action="{{ route('admin.translations.store') }}" method="post">
        <table class="table table-bordered table-striped mb-0">
            <tr>
                <th rowspan="2" class="align-middle bg-white">Орыс тілінде</th>
                @foreach($categories as $category)
                    <th colspan="{{ $category->children->count() }}"
                        @if(!$category->hasChildren())
                            rowspan="2"
                        @endif
                        class="text-center align-middle bg-white">
                        {{ $category->name }}
                    </th>
                @endforeach
            </tr>
            <tr>
                @foreach($categories as $category)
                    @if($category->hasChildren())
                        @foreach($category->children as $child)
                            <th>{{ $child->name }}</th>
                        @endforeach
                    @endif
                @endforeach
            </tr>
            @foreach($tnMap as $key => $tn)
                <tr>
                    <td>
                        <input type="text" name="ts[{{ $cid }}][{{ $key }}]"
                           value="{{ $tn['parent']->content }}" class="form-control">
                    </td>
                    @foreach($categories as $category)
                        @if($category->hasChildren())
                            @foreach($category->children as $child)

                                <td>
                                    <input type="text" name="ts[{{ $child->id }}][{{ $tn[$child->id]->id}}]"
                                           value="{{ $tn[$child->id]->content }}" class="form-control">
                                </td>
                            @endforeach
                        @else
                            <td>
                                <input type="text" name="ts[{{ $category->id }}][{{ $tn[$category->id]->id }}]"
                                       value="{{ $tn[$category->id]->content }}" class="form-control">
                            </td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
            <tr>
                <td>
                    <input type="text" name="new_parent" class="form-control">
                </td>
                @foreach($categories as $category)
                    @if($category->hasChildren())
                        @foreach($category->children as $child)
                            <td>
                                <input type="text" name="new_ts[{{ $child->id }}]"
                                       class="form-control">
                            </td>
                        @endforeach
                    @else
                        <td>
                            <input type="text" name="new_ts[{{ $category->id }}]"
                                   class="form-control">
                        </td>
                    @endif
                @endforeach
            </tr>
        </table>
        <button class="btn btn-primary">Submit</button>
        {{ csrf_field() }}
    </form>
@endsection