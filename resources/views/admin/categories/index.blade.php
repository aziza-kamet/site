@extends('layouts.admin')

@section('title')
    {{ __('main.categories') }}
@endsection

@section('content')
    @include('admin.categories.partials.form', [
        'type' => config('constants.types.forms.add'),
        'action' => route('admin.categories.store')
    ])
    <br>
    <div>
        @each('admin.categories.partials.nodes', $roots, 'category')
    </div>

    <div id="editModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('main.edit_category') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @include('admin.categories.partials.form', ['type' => config('constants.types.forms.edit')])
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#editModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var name = button.data('name');
            var action = button.data('action');

            var modal = $(this);
            modal.find('#name').val(name);
            modal.find('form').attr('action', action);
        });
    </script>
@endsection