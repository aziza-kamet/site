<li class="d-flex justify-content-between">
    {{ $category->name }}
    <div class="d-flex justify-content-between">
        <form action="{{ route('admin.categories.destroy', $category->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('delete') }}
            <button class="btn btn-danger">
                <span class="oi oi-x" title="Өшіру" aria-hidden="true"></span>
            </button>
        </form>
        <a class="btn btn-primary" data-toggle="modal" data-target="#editModal"
           data-name="{{ $category->name }}" data-action="{{ route('admin.categories.update', $category->id) }}">
            <span class="oi oi-pencil" title="Өзгерту" aria-hidden="true"></span>
        </a>
    </div>
</li>

@if($category->hasChildren())
    <ul>
        @foreach($category->children as $category)
            @include('admin.categories.partials.nodes', $category)
        @endforeach
    </ul>
@endif