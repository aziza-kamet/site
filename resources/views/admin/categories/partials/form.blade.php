<form action="{{isset($action) ? $action : null}}" method="post">
    <div class="form-group">
        <label for="name">{{ __('main.naming') }}</label>
        <input type="text" class="form-control" id="name" name="name">
    </div>
    @if( $type === config('constants.types.forms.add') )
        <div class="form-group">
            <label for="parent">{{ __('main.category') }}</label>
            <select name="parent" id="parent" class="form-control">
                <option value="0">{{ __('main.non_category') }}</option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
    @endif
    <button type="submit" class="btn btn-primary">{{ __('main.save') }}</button>
    {{csrf_field()}}
    @if( $type === config('constants.types.forms.edit') )
        {{method_field('put')}}
    @endif
</form>