@extends('layouts.admin')

@section('title')
    {{ __('main.profile') }}
@endsection

@section('content')
    <form method="POST" action="{{ route('admin.update') }}">
        <div class="form-group">
            <label for="username">{{ __('main.username') }}</label>
            <input id="username" type="text" class="form-control" name="login" value="{{ user()->login }}">
        </div>
        <div class="form-group">
            <label for="email">{{ __('main.email') }}</label>
            <input id="email" type="text" class="form-control" name="email" value="{{ user()->email }}">
        </div>
        <div class="form-group">
            <label for="name">{{ __('main.name') }}</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ user()->name }}">
        </div>
        <div class="form-group">
            <label for="surname">{{ __('main.surname') }}</label>
            <input id="surname" type="text" class="form-control" name="surname" value="{{ user()->surname }}">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                {{ __('main.save') }}
            </button>
        </div>

        {{ csrf_field() }}
        {{ method_field('put') }}
    </form>
@endsection
