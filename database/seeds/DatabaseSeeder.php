<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    private $tables = [
        'roles',
        'users',
        'publications',
        'categories',
        'posts',
        'comments',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->tables as $table) {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            DB::table($table)->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

        Role::insert([
            ['name' => 'admin'],
            ['name' => 'user'],
        ]);

        $admin = factory(User::class)->create([
            'email' => 'admin@mail.com',
            'login' => 'admin',
            'role_id' => Role::adminId()
        ]);

        $categoryNames = config('constants.categories');

        $categories = array_map(function ($name) {
            return [
                'name' => $name,
                'slug' => slug($name)
            ];
        }, $categoryNames);

        \App\Category::insert($categories);
    }

    public function mapCategoriesWithParent($array, $category)
    {
        return array_map(function ($name) use ($category) {
            return [
                'name' => $name,
                'parent_id' => $category->id,
                'slug' => slug($name)
            ];
        }, $array);
    }
}
