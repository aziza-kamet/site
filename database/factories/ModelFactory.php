<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'login' => $faker->userName,
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'role_id' => \App\Role::userId(),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->word,
        'slug' => slug($name)
    ];
});

$factory->define(App\Publication::class, function (Faker $faker) {
    return [
        'name' => today()->format('d-m-y')
    ];
});

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'category_id' => function () {
            return factory(\App\Category::class)->create();
        },
        'publication_id' => function () {
            if (\App\Publication::existsToday()) {
                return \App\Publication::today();
            }
            return factory(\App\Publication::class)->create();
        },
        'user_id' => function () {
            return factory(\App\User::class)->create([
                'role_id' => \App\Role::adminId()
            ]);
        },
        'author' => $faker->name,
        'title' => $faker->sentence,
        'content' => $faker->text
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'post_id' => function () {
            return factory(\App\Post::class)->create();
        },
        'author_id' => function () {
            return factory(\App\User::class)->create();
        },
        'content' => $faker->text
    ];
});
