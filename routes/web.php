<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['admin'])->group(function () {
    Route::name('admin.')->group(function () {
        Route::prefix('admin')->group(function () {

            Route::get('/', 'PageController@dashboard')->name('dashboard');
            Route::put('/update', 'UserController@updateAdmin')->name('update');

            Route::resource('categories', 'CategoryController', [
                'except' => [
                    'create',
                    'edit',
                    'show'
                ]
            ]);
            Route::resource('posts', 'PostController', [
                'except' => [
                    'create',
                    'edit'
                ]
            ]);

            Route::resource('translations', 'TranslationController', [
                'except' => [
                    'create',
                    'edit',
                    'show'
                ]
            ]);

            Route::get('/users', 'UserController@index')->name('users.index');

            Route::delete('/comments/{id}', 'CommentController@destroy')->name('comments.destroy');
        });
    });
});

Route::middleware(['categories'])->group(function () {

    Route::get('/', 'PageController@index')->name('home');

    Route::get('/categories/{id}/posts', 'CategoryController@posts')->name('category.posts');

    Route::get('/posts/{id}', 'PostController@show')->name('posts.show');
    Route::post('/posts/{pid}/comments', 'CommentController@store')->name('post.comments.store');

    Route::get('/profile', 'PageController@profile')->name('profile');

    Route::put('/users', 'UserController@update')->name('users.update');
});

Route::prefix('json')->group(function () {
    Route::get('/posts/{id}', 'PostController@jsonShow');
});
